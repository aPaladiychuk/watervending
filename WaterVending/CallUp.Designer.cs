﻿namespace WaterVending
{
    partial class CallUp
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CallUp));
            this.label1 = new System.Windows.Forms.Label();
            this.tb_NetSig = new System.Windows.Forms.TextBox();
            this.tb_imei = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.bt_connect = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.bt_request = new System.Windows.Forms.Button();
            this.tGsmCheck = new System.Windows.Forms.Timer(this.components);
            this.tb_WorkMode = new System.Windows.Forms.TextBox();
            this.bt_disconect = new System.Windows.Forms.Button();
            this.dw_units = new Sybase.DataWindow.DataWindowControl();
            this.sqlca = new Sybase.DataWindow.Transaction(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.lvStat = new System.Windows.Forms.ListView();
            this.hPhone = new System.Windows.Forms.ColumnHeader();
            this.hAddress = new System.Windows.Forms.ColumnHeader();
            this.hAction = new System.Windows.Forms.ColumnHeader();
            this.hTime = new System.Windows.Forms.ColumnHeader();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Сеть";
            // 
            // tb_NetSig
            // 
            this.tb_NetSig.Location = new System.Drawing.Point(73, 9);
            this.tb_NetSig.Name = "tb_NetSig";
            this.tb_NetSig.Size = new System.Drawing.Size(349, 20);
            this.tb_NetSig.TabIndex = 1;
            // 
            // tb_imei
            // 
            this.tb_imei.Location = new System.Drawing.Point(73, 44);
            this.tb_imei.Name = "tb_imei";
            this.tb_imei.Size = new System.Drawing.Size(349, 20);
            this.tb_imei.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "IMEI";
            // 
            // bt_connect
            // 
            this.bt_connect.Location = new System.Drawing.Point(445, 6);
            this.bt_connect.Name = "bt_connect";
            this.bt_connect.Size = new System.Drawing.Size(106, 23);
            this.bt_connect.TabIndex = 4;
            this.bt_connect.Text = "Подключение";
            this.bt_connect.UseVisualStyleBackColor = true;
            this.bt_connect.Click += new System.EventHandler(this.button1_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(1, 482);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(260, 108);
            this.listBox1.TabIndex = 5;
            // 
            // bt_request
            // 
            this.bt_request.Location = new System.Drawing.Point(445, 67);
            this.bt_request.Name = "bt_request";
            this.bt_request.Size = new System.Drawing.Size(106, 23);
            this.bt_request.TabIndex = 7;
            this.bt_request.Text = "Опрос устройств";
            this.bt_request.UseVisualStyleBackColor = true;
            this.bt_request.Click += new System.EventHandler(this.bt_request_Click);
            // 
            // tGsmCheck
            // 
            this.tGsmCheck.Interval = 120000;
            this.tGsmCheck.Tick += new System.EventHandler(this.tGsmChack_Tick);
            // 
            // tb_WorkMode
            // 
            this.tb_WorkMode.Enabled = false;
            this.tb_WorkMode.Location = new System.Drawing.Point(12, 69);
            this.tb_WorkMode.Name = "tb_WorkMode";
            this.tb_WorkMode.Size = new System.Drawing.Size(410, 20);
            this.tb_WorkMode.TabIndex = 9;
            // 
            // bt_disconect
            // 
            this.bt_disconect.Location = new System.Drawing.Point(573, 7);
            this.bt_disconect.Name = "bt_disconect";
            this.bt_disconect.Size = new System.Drawing.Size(108, 23);
            this.bt_disconect.TabIndex = 10;
            this.bt_disconect.Text = "Закрыть порт";
            this.bt_disconect.UseVisualStyleBackColor = true;
            this.bt_disconect.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // dw_units
            // 
            this.dw_units.DataWindowObject = "d_units_call";
            this.dw_units.Icon = ((System.Drawing.Icon)(resources.GetObject("dw_units.Icon")));
            this.dw_units.LibraryList = "watervending.pbl";
            this.dw_units.Location = new System.Drawing.Point(12, 124);
            this.dw_units.Name = "dw_units";
            this.dw_units.ScrollBars = Sybase.DataWindow.DataWindowScrollBars.Both;
            this.dw_units.Size = new System.Drawing.Size(816, 352);
            this.dw_units.TabIndex = 11;
            this.dw_units.Text = "dataWindowControl1";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 95);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "Выделить все";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(158, 95);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(86, 23);
            this.button2.TabIndex = 13;
            this.button2.Text = "Отменить все";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(606, 95);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(159, 23);
            this.button3.TabIndex = 15;
            this.button3.Text = "Принудительній сброс";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // lvStat
            // 
            this.lvStat.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.hPhone,
            this.hAddress,
            this.hAction,
            this.hTime});
            this.lvStat.Location = new System.Drawing.Point(267, 482);
            this.lvStat.Name = "lvStat";
            this.lvStat.Size = new System.Drawing.Size(561, 109);
            this.lvStat.TabIndex = 16;
            this.lvStat.UseCompatibleStateImageBehavior = false;
            this.lvStat.View = System.Windows.Forms.View.Details;
            // 
            // hPhone
            // 
            this.hPhone.Text = "Телефон";
            this.hPhone.Width = 96;
            // 
            // hAddress
            // 
            this.hAddress.Text = "Адрес";
            this.hAddress.Width = 132;
            // 
            // hAction
            // 
            this.hAction.Text = "действие";
            this.hAction.Width = 66;
            // 
            // hTime
            // 
            this.hTime.Text = "время";
            this.hTime.Width = 95;
            // 
            // CallUp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(840, 602);
            this.Controls.Add(this.lvStat);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dw_units);
            this.Controls.Add(this.bt_disconect);
            this.Controls.Add(this.tb_WorkMode);
            this.Controls.Add(this.bt_request);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.bt_connect);
            this.Controls.Add(this.tb_imei);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tb_NetSig);
            this.Controls.Add(this.label1);
            this.Name = "CallUp";
            this.Text = "Сбор данных";
            this.Load += new System.EventHandler(this.CallUp_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CallUp_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_NetSig;
        private System.Windows.Forms.TextBox tb_imei;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bt_connect;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button bt_request;
        private System.Windows.Forms.Timer tGsmCheck;
        private System.Windows.Forms.TextBox tb_WorkMode;
        private System.Windows.Forms.Button bt_disconect;
        private Sybase.DataWindow.DataWindowControl dw_units;
        public Sybase.DataWindow.Transaction sqlca;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ListView lvStat;
        private System.Windows.Forms.ColumnHeader hPhone;
        private System.Windows.Forms.ColumnHeader hAddress;
        private System.Windows.Forms.ColumnHeader hAction;
        private System.Windows.Forms.ColumnHeader hTime;
    }
}