﻿    using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;
using iAnywhere.Data.AsaClient;
using log4net;
using log4net.Config;

namespace WaterVending
{
     /*
         *
         * AT+CHUP отклонить входящий вызов
                ATA принять входящий вызов
                AT+CPAS состояние телефона, 0-режим ожидания, 3-входящий вызов
                AT+CLCC при входящем звонке показывает номер звонящего
                AT^SMSO выключить телефон
                AT+GSN показать IMEI телефона
                AT+CBC уровень заряда батареи, показывает в процентах, 1 - 100%
                AT+CGMI производитель модема
                AT+CGMM модель модема
                AT+CGMR версия прошивки
                AT+CSQ уровень приема сигнала от базовой станции
                AT+CIMI IMSI номер сим карты
         */

    public partial class CallUp : Form
    {
        const int PHONE_READY = 1;
        const int PHONE_RING = 2;
        const int PHONE_CALL = 4;
        const int PHONE_RECIVE = 8; 
        const int PHONE_FAILED = 0;
        int Phone_Stat;
        int Phone_Mod;
        
        Dictionary<String, String> Param;


        Queue<string> comData; 

        SerialPort port;
        string port_num;
        string boundRate;
        ArrayList retVal;
        String CurrentAction;
        String CurrentCommand;
        String sTime;
        DateTime dt_request;
        Boolean isRing = false;
        String LastRet;
        String Result;
        //===================================================
        iAnywhere.Data.AsaClient.AsaConnection connection  ;
        String sqlCheckPhoneNumber = "select nId, vaddress  from apo.tUnits where vPhone =:a_phone";

        int a_nid;
        String a_vAction, a_vStatus;

        public static readonly ILog logger = LogManager.GetLogger(typeof(CallUp));
        //===================================================
        public CallUp()
        {
            InitializeComponent();
       //     Modem = PortInterface.Instance;
            retVal = new ArrayList();
            Param = new Dictionary<string, string>();
            try
            {
                connection = new AsaConnection("uid=dba;pwd=sql;dsn=water");
                connection.Open();
            }
            catch (AsaException ae)
            {
                logger.Error(ae.Message, ae);
            }
            catch (Exception ee1)
            {
                logger.Error("Error in Connect "+ ee1.Message, ee1);
            }

            comData = new Queue<string>();
            try
            {
                dbLoadParameters();
            }
            catch (Exception ee)
            {
                logger.Error("Unknow Exception " + ee.Message, ee);
            }


        }
        public Boolean Call(String _number, int _id , string _type , string _address )
        {
            ArrayList Val;
            DbVendingParam data;
            
            Phone_Stat = PHONE_CALL;
            retVal.Clear();
            port.DataReceived -= new SerialDataReceivedEventHandler(com_Reciver);
            tGsmCheck.Stop();
            port.WriteLine("AT");
            Thread.Sleep(100);
            Val = ReadFromComPort("OK");
            
            retVal.Clear();
            port.WriteLine("ATD" + _number);
            Thread.Sleep(10000);
            Val = ReadFromComPort("CONNECT 9600");

            logger.Info("Опрос оборудования " + _number );

            if (Val.IndexOf("CONNECT 9600") == -1 )
            {
               //Error    
                dbSaveAction(_id, _number, "CALL", "ERROR");
                port.DataReceived += new SerialDataReceivedEventHandler(com_Reciver);
                tGsmCheck.Start();
                return false ; 
            } 
            if (Val.IndexOf("C>\r") == -1)            
            {
                Thread.Sleep(1000);
                Val.Clear();
                Val = ReadFromComPort("C>");

            }

            if (Val.IndexOf("C>\r") > 0)
            {
                // есть приглашение запрашиваем данные 
                //Val.Clear();
                //Val = ReadFromPort("C>");

                port.WriteLine("GET_DATA");
                Thread.Sleep(2000);
                Val = ReadFromComPort("C>");



                data = new DbVendingParam(_id ,_type , connection, Param  );
               // data.Param = Param ; 
                for (int i = 0; i < Val.Count; i++)
                {
                    data.ParseParam(Val[i].ToString() );
                    logger.Info(Val[i].ToString());
                }
                if (data.sAlarm.Equals("0") && data.sStatus.Equals("6") ) 
                {
                   
                    lvStat.Items.Add(new ListViewItem(new String[] { _number, _address, "Взлом",  DateTime.Now.ToString("dd.MM.yy hh:mm:ss")  }));
                }
                if ((data.sAlarm.Equals("1") || data.sAlarm.Equals("20") ) && data.sStatus.Equals("6"))
                {
                   
                    lvStat.Items.Add(new ListViewItem(new String[] { _number, _address, "Обслуживание", DateTime.Now.ToString("dd.MM.yy hh:mm:ss") }));
                }
                if (data.sStatus.Equals("9") || data.sAlarm.Equals("10"))
                {
                   
                    lvStat.Items.Add(new ListViewItem(new String[] { _number, _address, "Закончилась вода", DateTime.Now.ToString("dd.MM.yy hh:mm:ss") }));
                }
                data.SaveParam();
                for (int i = 0; i < data.set_command.Count; i++)
                {
                    port.WriteLine(data.set_command[i].ToString() );
                    Val = ReadFromComPort("C>");
                }
                data.set_command.Clear();

                port.WriteLine("EXIT");
                Thread.Sleep(2000);
                ReadFromComPort("NO CARRIER");
            }
            port.DataReceived += new SerialDataReceivedEventHandler(com_Reciver);
            tGsmCheck.Start();
            return true;
        }
        
        private ArrayList ReadFromPort(String sWaitFor ){
            ArrayList _ret;
            String s = "";
            int cnt = 0;
            int len;
            len = sWaitFor.Length; 
            _ret = new ArrayList();
            while (s.IndexOf(sWaitFor) == -1)
            {

                while (comData.Count > 0)
                {
                    try
                    {
                      //  s = port.ReadLine();
                        s = comData.Dequeue();
                        if (s.Length > 0)
                        {
                            _ret.Add(s);
                            listBox1.Items.Insert(0, s);
                            if( sWaitFor.Equals("CONNECT 9600" ) ) {
                                    if (s.IndexOf("NO CARRIER") != -1)
                                        return _ret;
                            }
                            if (s.IndexOf(sWaitFor) != -1)
                                return _ret;
                        }
                    }
                    catch (System.TimeoutException ee)
                    { }
                }
                
                Thread.Sleep(500);
                cnt++;
                if (cnt > 10) break;
            }
              return _ret;            

        }
        private ArrayList ReadFromComPort(String sWaitFor)
        {
            ArrayList _ret;
            String s = "";
            int cnt = 0;
            int len;
            len = sWaitFor.Length;
            _ret = new ArrayList();
            while (s.IndexOf(sWaitFor) == -1)
            {

                while (port.BytesToRead > 0)
                {
                    try
                    {
                        s = port.ReadLine();
                        //s = comData.Dequeue();
                        if (s.Length > 0)
                        {
                            if (s.IndexOf("CONNECT 9600") != -1) s = "CONNECT 9600";
                            _ret.Add(s);
                            
                            listBox1.Items.Insert(0, s);
                            if (sWaitFor.Equals("CONNECT 9600"))
                            {
                                if (s.IndexOf("NO CARRIER") != -1)
                                    return _ret;

                            }
                            if (s.IndexOf(sWaitFor) != -1)
                                return _ret;
                        }
                    }
                    catch (System.TimeoutException ee)
                    { }
                }

                Thread.Sleep(500);
                cnt++;
                if (cnt > 10) break;
            }
            return _ret;

        }

        public void DataReciver(string _command, string _value)
        {
            String buf;
            switch (_command)
            {
                case "RING":
                    listBox1.Items.Add(_value);
                    buf = _value.Substring(_value.IndexOf("+", 5), 13);
                    //Call(buf,1,"");
                    break;
                case "AT+GSN":
                    {
                        tb_imei.Text = _value;
                    }
                    break;

                case "AT+CSQ":
                        tb_NetSig.Text = _value;
                    break;
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Init("");
            if (Phone_Stat == PHONE_READY)
            {
                tb_WorkMode.Text = "Режим ожидания";
                ArrayList Val;
                port.WriteLine("AT+CSQ");
                Thread.Sleep(100);
                Val = ReadFromPort("OK");
                if ( Val.Count > 0 ) 
                        this.DataReciver("AT+CSQ", Val[Val.Count -2].ToString() );

                port.WriteLine("AT+GSN");
                Thread.Sleep(100);
                Val = ReadFromPort("OK");
                if (Val.Count > 0) 
                    this.DataReciver("AT+GSN", Val[Val.Count -2].ToString() );

               
            }
        }


        public void Init(string _type  )
        {

            port_num = Param["COMPORT"];
            boundRate = Param["BOUNDRATE"];
            sTime = Param["TIME"];
            
            Phone_Stat = PHONE_READY;
            port_num = Param["COMPORT"];
            if (port != null  )
            {
                if (port.IsOpen) port.Close(); 
            }
            port = new System.IO.Ports.SerialPort();

            //далее необходимо настроить порт для работы с мобильным телефоном
            port.PortName = port_num;

            //Время ожидания записи и чтения с порта
            port.WriteTimeout = 500; port.ReadTimeout = 500;

            //Настраиваем скорость обмена данными с телефоном - телефон не может обрабатывать данный на максимальной скорости
            port.BaudRate = System.Convert.ToInt32(boundRate);

            //Другие необходимые настройки - подходит для большинства телефонов - но возможно придется настраивать:
          
            port.Parity = Parity.None;
            port.DataBits = 8;
            port.StopBits = StopBits.One;
            port.Handshake = Handshake.RequestToSend;
            port.DtrEnable = true;
            port.RtsEnable = true;
            port.NewLine = System.Environment.NewLine;
           port.DataReceived += new SerialDataReceivedEventHandler(com_Reciver);

            //открываем порт
           logger.Info("Инициализация порта");
            try
            {
                port.Open();
             //   WriteCommand("AT");
                logger.Info("Инициализация порта - успешно");
            }
            catch (System.IO.IOException e)
            {
                tb_imei.Text = e.Message ;
                Phone_Stat = PHONE_FAILED;
                logger.Error("Ошибка открытия порта");
            }

            ChangeStatus();
        }
        private void ChangeStatus()
        {
            if (Phone_Stat == PHONE_FAILED)
            {
                bt_request.Enabled = false;

                bt_connect.Enabled = true;
                tGsmCheck.Stop();
            }
            else
            {
                bt_request.Enabled = true;

                bt_connect.Enabled = false;
                tGsmCheck.Start();
            }
        }
        public void RingUp()
        {
            String _number;
            ArrayList Val;
            String sRetVal;
            tb_WorkMode.Text =  " S";
            WriteCommand("AT+CLCC");
            logger.Info("Входящий звонок");
            Val = ReadFromPort("OK");
            sRetVal = "";
            for (int i = 0; i < Val.Count; i++)
            {
                if (Val[i].ToString().IndexOf("+CLCC:") != -1)
                {
                    sRetVal = Val[i].ToString();
                    break;
                }
            }

            if (sRetVal.IndexOf("+CLCC:") != -1 )
            {
                
                Phone_Stat = PHONE_READY;
                listBox1.Items.Add(sRetVal);
                listBox1.Items.Add(sRetVal.Substring(sRetVal.IndexOf("+", 5), 13));
                _number = sRetVal.Substring(sRetVal.IndexOf("+", 5), 13);
                Thread.Sleep(10000);
                WriteCommand("AT+CHUP");
                Val = ReadFromPort("OK");
                WriteCommand("AT");
                Val = ReadFromPort("OK");

                if (Val.IndexOf("OK") > 0 )
                {

                    isRing = false;
                    Int32 nUnit = 0;
                    String sGround;
                    String sAddress = "";
                    String sStat = "ERR" ;
                    AsaCommand cmd = new AsaCommand(sqlCheckPhoneNumber, connection);
                    cmd.Parameters.Add("a_phone",_number );

                    AsaDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        nUnit = dr.GetInt32(0);
                        sAddress = dr.GetString(1);

                        sStat = "OK";
                    }

                    if (sStat.Equals("OK"))
                    {
                        Thread.Sleep(15000); // Ждем 15 сек пока устаканится сеть- !!! проверить на модеме нужно ли                        
                        Call(_number, nUnit, "", sAddress);
                    }
                    else
                    {
                        logger.Error("Неопознанній номер " + _number );
                        dbSaveAction(0, _number, "RINGUP", "ERROR");
                    }

                }
                isRing = false;
                CurrentAction = "READY";
            }






        }
        public void WriteCommand(String _command)
        {
            int i = 0;
            String sData;
            CurrentAction = "ASK";
            retVal.Clear();
            LastRet = "";
            Result = "";
            port.WriteLine(_command);
        /*    while  (Phone_Stat == PHONE_CALL)
            {
                ////  обработка дозвона
                if (LastRet.Equals("NO CARRIER"))
                {
                    Phone_Stat = PHONE_READY;
                    break  ;
                }
                if ( LastRet.Equals("CONNECT 9600") ) 
                {

                }

            } */
            // !LastRet.Equals("BISY") || !LastRet.Equals("CONNECT") || !LastRet.Equals("NO CARRIER")
            /*while  (Phone_Mod == PHONE_RECIVE ) ///(!LastRet.Equals("OK") )
            {
                Thread.Sleep(10);
                i++;
                if (i > 100)
                {
                    CurrentAction = "ERR";
                //    return;
                }
                //Thread.Sleep(500);
            }
                while (comData.Count > 0 )
                {
                    sData = comData.Dequeue();
                    if (sData.Length == 0) continue;
                    else
                        if (sData.Equals("OK"))
                        {
                            if (Result.Length == 0) Result = sData;
                        }
                        else
                        {
                            Result = sData;
                        }


                }
            */
            CurrentAction = "READY";


        }
        private void com_Reciver(object sender, SerialDataReceivedEventArgs e)
        {

            String sRet;
            sRet = "";
            //System.IO.Ports.SerialPort port1 = (System.IO.Ports.SerialPort)sender;
            //long l = port1.BytesToRead;
            long cnt = 0;
      //      if (Phone_Mod == PHONE_CALL) return;
            Phone_Mod = PHONE_RECIVE; 
            while (port.BytesToRead > 0)
            {
                try
                {
                    sRet = port.ReadLine();
                }
                catch (System.TimeoutException te)
                {
                    cnt++;
                }
              
                if (sRet.Length > 0)
                {
                    if (sRet.Equals("RING") && isRing) continue ;
                    LastRet = sRet;
                    retVal.Add(sRet);
                    comData.Enqueue(sRet);
                } 
               
             
                if (sRet.Length >= 4)
                {
                    if (sRet.Equals("RING") && !isRing )
                    {
                        isRing = true;
                        Phone_Stat = PHONE_RING;
                        Phone_Mod = PHONE_READY; 
                        this.BeginInvoke(new EventHandler(delegate { this.RingUp(); }));
                        return;
                    }
                }
              
                
                // Result += sRet;
            }
           

            CurrentCommand = "";
            CurrentAction = "READY";
            Phone_Mod = PHONE_READY; 
        }

        public void CallText(object data )
        {

        }

        /*private void RequstData()
        {
            string sql = @"select nid , vphone , vname , vserno
                            from apo.tUnits where vstat = 'OK' ";
            Int32 nId;
            String sPhone, sSerNo;

            AsaCommand cmd = new AsaCommand(sql, connection);
            AsaDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                nId = dr.GetInt32(0);
                sPhone = dr.GetString(1);
                sSerNo = dr.GetString(3);
                Call(sPhone, nId, "REQUST");
            }
        }
         * */
        private void dbLoadParameters()
        {
            String sqlLoadParam = @"select vtype, vValue from apo.tParameter";
            String sId, sValue;
            AsaCommand cmd = new AsaCommand(sqlLoadParam, connection);
            AsaDataReader dr = cmd.ExecuteReader();
            Param.Clear();
            while (dr.Read())
            {
                sId = dr.GetString(0);
                sValue = dr.GetString(1);
                Param.Add(sId, sValue);
            }

            dr.Close();

        }
        
        private void dbSaveAction(int _id, string _number, string  _action , string _stat )
        {
            String sqlInsertSessionLog = "insert into apo.session_log( nId,dtAction,vAction,vStatus,vGround)" +
                            " values (:a_nid,current timestamp ,:a_vAction,:a_vstatus,:a_vGround )";
            String sGround, sStat ;
            AsaCommand cmd = new AsaCommand(sqlInsertSessionLog, connection);
            if (_id == 0)
            {
                _id = 0;
                sGround = "Неизвестный номер " + _number;
                sStat = "ERROR";
            }
            else
            {
                sGround = "Номер  определен" + _number;
                sStat = "OK";
            }
            cmd.Parameters.Add("a_nid", _id );
            cmd.Parameters.Add("a_vAction", _action);
            cmd.Parameters.Add("a_vstatus", sStat);
            cmd.Parameters.Add("a_vGround", sGround);
            
            cmd.ExecuteNonQuery();

        }
        private int dbCheckId(String _phoneNo)
        {
            int _id = 0 ;
            AsaCommand cmd = new AsaCommand(sqlCheckPhoneNumber, connection);
            cmd.Parameters.Add("a_phone", _phoneNo);
            AsaDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                _id = dr.GetInt32(0);
            }
            return _id;
        }

        private void bt_request_Click(object sender, EventArgs e)
        {
            RequastByList();
        }

        private void RequastByList(){
            String vName, vSerno, vPfone, sAddress ;
            Int32 nId;    
            int curRow;
            
            curRow = 0 ;
            tb_WorkMode.Text = "Режим опрос устройств";
            for (int i = 1; i < dw_units.RowCount; i++)
            {              
                dw_units.SetItemDecimal(i, "ncallcnt", 0);
                dw_units.SetItemString(i, "vcallstat", "");
            }
            curRow = dw_units.FindRow("isCall = 1 and vcallstat <> 'OK' and ncallcnt < 5 ", curRow + 1, dw_units.RowCount);
            while ( curRow > 0 ) 
            {

                vPfone = dw_units.GetItemString(curRow, "vPhone");
                sAddress = dw_units.GetItemString(curRow, "vAddress");
                nId = System.Convert.ToInt32(dw_units.GetItemDecimal(curRow, "nid"));

                listBox1.Items.Add( vPfone);
                dw_units.SetItemDecimal(curRow, "ncallcnt", dw_units.GetItemDecimal(curRow,"ncallcnt") + 1);

                if (Call(vPfone, nId, "REQUST",sAddress) ) 
                {
                    dw_units.SetItemString(curRow, "vcallstat","OK" );
                }
                else
                {
                    dw_units.SetItemString(curRow, "vcallstat", "NC");
                }
                if (curRow == dw_units.RowCount) curRow = 0;
                curRow = dw_units.FindRow("isCall = 1 and vcallstat <> 'OK' and ncallcnt < 3 ", curRow + 1, dw_units.RowCount);

            }

            listBox1.Items.Add("Опрос завершен");
            tb_WorkMode.Text = "Режим ожидания";

            /*

            connection = new AsaConnection("uid=dba;pwd=sql;dsn=water");
            connection.Open();

            DataTable dt;
            String sqlListDevice;

            sqlListDevice = @"select nid, vname ,vSerno, vPhone 
                    from apo.tUnits 
                    where nid >  0 and isnull(vPhone,'') <> '' and vstat  = 'OK'
                    order by vSerno";
            AsaCommand cmd = new AsaCommand(sqlListDevice, connection);
            AsaDataReader dr = cmd.ExecuteReader();
            dt = new DataTable();

            dt.Load(dr);

          
           foreach(DataRow row in dt.Rows) {
                
                vName = row[dt.Columns["vname"]].ToString();
                vSerno = row[dt.Columns["vSerno"]].ToString();
                vPfone = row[dt.Columns["vPhone"]].ToString();
                nId = System.Convert.ToInt32(row[dt.Columns["nid"]].ToString());

                listBox1.Items.Add(vName + "  " + vPfone);
                Call(vPfone, nId , "REQUST");
            }
            */

        }
        private void CallUp_Load(object sender, EventArgs e)
        {
            dw_units.SetTransaction(sqlca);
            dw_units.Retrieve();         
        }

     
        private void tGsmChack_Tick(object sender, EventArgs e)
        {
              ArrayList Val;
              DateTime dt_cur;
              dt_cur = DateTime.Now;
              try
              {
                  dt_request = DateTime.ParseExact(sTime, "HH:mm", System.Globalization.CultureInfo.CurrentCulture);
                  tb_imei.Text = "dt_request OK  = " + dt_request.ToString(); 
              }
              catch (Exception ee)
              {
                  
                  tb_imei.Text = ee.Message + " dt =" + dt_request.ToString();
                  
              }
              double  min =  (dt_request - dt_cur).TotalMinutes;
              if (min > 0 && min < 5)
              {
                  for (int i = 1; i <= dw_units.RowCount; i++)
                  {
                      dw_units.SetItemDecimal(i, "isCall", 1);
                  }
                  RequastByList();
              }
              port.WriteLine("at+cusd=1,\"*111#\",15");
               Thread.Sleep(100);
                Val = ReadFromPort("OK");
                if (Val.Count > 0)
                {
//                    tb_NetSig.Text = Val[Val.Count - 2].ToString() +"  "+  DateTime.Now.ToString("HH:mm:ss");
                }
                Val = ReadFromPort("+CUSD");
                if (Val.Count > 0)
                {
                    tb_NetSig.Text = dt_cur.ToString("dd.MM.yy HH:mm:ss") + "  " +   Val[0].ToString();
                }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            tGsmCheck.Stop();
            port.Close();
            port.Dispose();
            tb_WorkMode.Text = "Порт закрыт";
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            for (int i = 1; i <= dw_units.RowCount; i++)
            {
                dw_units.SetItemDecimal(i, "isCall", 1);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            for (int i = 1; i <= dw_units.RowCount; i++)
            {
                dw_units.SetItemDecimal(i, "isCall", 0);
                dw_units.SetItemDecimal(i, "ncallcnt", 0);
                dw_units.SetItemString(i, "vcallstat", "");

            }
        }

        private void CallUp_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                port.Close();
                port.Dispose();
            }
            catch (Exception ee)
            {
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            int curRow;
            String vPfone , sAddress ;
            int nId;
             curRow = dw_units.FindRow("isCall = 1 and vcallstat <> 'OK' and ncallcnt < 3 ", 1, dw_units.RowCount);
             if (curRow > 0)
             {
                 
                vPfone = dw_units.GetItemString(curRow, "vPhone");
                sAddress = dw_units.GetItemString(curRow, "vAddress");
                nId = System.Convert.ToInt32(dw_units.GetItemDecimal(curRow, "nid"));

                listBox1.Items.Add( vPfone);
                dw_units.SetItemDecimal(curRow, "ncallcnt", dw_units.GetItemDecimal(curRow,"ncallcnt") + 1);

                if (Call(vPfone, nId, "RESET", sAddress))
                {
                    dw_units.SetItemString(curRow, "vcallstat", "OK");
                }
                else
                {
                    dw_units.SetItemString(curRow, "vcallstat", "NC");
                }
             }

        }

    }
}

/*
 * Протокол обмена:
1 – Инициатор обмена – сервер
- Набор номера
- Ожидание Connect
- Ожидание C>
- Процесс обмена
- Окончание обмена
2 – Инициатор обмена – станция
- Опрос номера абонента, если есть номер абонента в базе то сброс и
Инициатор связи – сервер



Команды:
GET_DATA – Выдает установленные данные

DATE- DD/MM/YY
TIME- HH/MM/SS
PRC1 -  99
PRC2 -  99
WHOL -  99
TARA –  999
DISR –  99999
MES1 – Сообщение 1
MES2 – Сообщение 2
…
MES9 – Сообщение 9
BALW- Остаток воды (Гарантированный)
KOPN – кол-во копеек в монетоприемнике
GRNN – кол-во гривен в купюроприемнике
SALW – Воды продано




SET_DATE – Устанавливает дату      DD/MM/YY
SET_TIME – Устанавливает время    HH/MM/SS
SET_PRC1 – Цена 1                             00 – 99  коп
SET_PRC2 – Цена 2                             00 – 99  коп
SET_WHOL – Граница оптовой цены  00 – 99  грн
SET_TARA – Емкость тары                   000 – 999 литр
SET_BALW – Запас остатка воды          00- 99 литр
SET_DISR – Дискретность счетчика     00000 – 00000 нанолитр

SET_MES1 – Сообщение 1
SET_MES2 – Сообщение 2

PUT_EXIT – Окончание сеанса связи


 */

/*
 *   
        System.IO.Ports.SerialPort port;
            port = new System.IO.Ports.SerialPort();

            //далее необходимо настроить порт для работы с мобильным телефоном
            port.PortName = "COM4";

            //Время ожидания записи и чтения с порта
            port.WriteTimeout = 500; port.ReadTimeout = 500;

            //Настраиваем скорость обмена данными с телефоном - телефон не может обрабатывать данный на максимальной скорости
            port.BaudRate = 9600;

            //Другие необходимые настройки - подходит для большинства телефонов - но возможно придется настраивать:
            port.BaudRate = 9600;
            port.Parity = Parity.None;
            port.DataBits = 8;
            port.StopBits = StopBits.One;
            port.Handshake = Handshake.RequestToSend;
            port.DtrEnable = true;
            port.RtsEnable = true;
            port.NewLine = System.Environment.NewLine;
            port.DataReceived += new SerialDataReceivedEventHandler(com_Reciver);

            //открываем порт
            port.Open(); 
           

            //В зависимости от телефона возможно необходимо задерживать обмен данными
            System.Threading.Thread.Sleep(500);

            //Далее можем работать с телефоном посредством AT команд
            //К примеру, набор номера
        //    port.WriteLine("ATA");
        //    String ls_ret;
            //ls_ret = port.ReadLine();
            port.WriteLine("ATD+*100#");
            System.Threading.Thread.Sleep(500);
            ls_ret = port.ReadLine();

            //Положить трубку
            port.WriteLine("ATH");
            
            //И конечно же, не забываем закрывать порты
           
        }

        private void button2_Click(object sender, EventArgs e)
        {
            String sParam, sRet;
            sParam = textBox1.Text;
            port.WriteLine(sParam);
            System.Threading.Thread.Sleep(500);
        //    long l = port.BytesToRead;
            //byte[] mBytes = new byte[l];
            //port.Read(mBytes, 0,(int) l);

            //sRet = System.Text.Encoding.Default.GetString(mBytes);
         //   sRet = port.ReadLine();
            //listBox1.Items.Add(sRet);
         
            listBox1.Items.Insert(0,sParam);
         
         
        }

        private void Form1_Deactivate(object sender, EventArgs e)
        {
           
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            port.Close(); 
        }
        private void com_Reciver(object sender, SerialDataReceivedEventArgs e)
        {
            String sRet;
            System.IO.Ports.SerialPort port1 = ( System.IO.Ports.SerialPort ) sender;
               long l = port1.BytesToRead;
            byte[] mBytes = new byte[l];
            port1.Read(mBytes, 0,(int) l);

            sRet = System.Text.Encoding.Default.GetString(mBytes);
         //   sRet = port.ReadLine();
              
            
            
            listBox1.Invoke(new EventHandler(delegate
            {
                this.listBox1.Items.Insert(0,sRet);    
            }));
            // port1.ReadExisting();
        }
    }
 * 
 * 
 */
