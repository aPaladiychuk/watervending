﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using iAnywhere.Data.AsaClient;
/*
 * ' 1 блок - (1,2) - сумма купюр в гривнах     - 0
' 2 блок - (1,2) - сумма монет в копейках    -0
' 3 блок - (1.2) - расход воды в дискретах счетчика   -0
' 4 блок - (1.2) - Емкомть накопителя воды в литрах   -const
' 5 блок - (1,2) - Дискрета счетчика в микролитрах (15625)    -const
' 6 блок - (1) - Цена в копейках   -var
' 7 блок - (1) - Оптлвая цена в копейках  -var
' 8 блок - (1) - Нижняя граница оптовой цены в рублях   -var
' 9 блок - (1) - Гарантированный остаток воды в литрах   -const
[16:58:36] Sergey Mozol: ' режим 1 - Дежурный режим прием купюр
' режим 2 - Режим выдачи воды
'
' режим 5 - запрос на техническое обслуживание (открыта дверь)
' режим 6 - ожидание вызова сервера
' режим 7 - разрешение техническое обслуживание
' режим 8 - техническое обслуживание
'
' режим 10 - Остаток воды меньше гарантированного
 * */


namespace WaterVending
{
    class DbVendingParam
    {
        Int32 nId;
        string sType;
        public string sAlarm;
        AsaConnection con;

        public ArrayList set_command;

        Decimal fPrice1, fPrice2;
        string sDate, sTime;
        Int32 iWhol, iTara, iDiscr;
        Int32 iBalW;
        Int32 iKopN, iGrnN;
        Decimal fSum; 
        Decimal fSalW;
        public Dictionary<String, String> Param;
        public string sStatus;
        decimal def_fPrice1, def_fPrice2;        
        int def_iWhol, def_iTara, def_iDiscr, def_iBalW;
        



/*          -- DATE- DD/MM/YY
         --   TIME- HH/MM/SS
       --     PRC1 -  99
         --   PRC2 -  99
           -- WHOL -  99
    --        TARA –  999
    --        DISR –  99999
            MES1 – Сообщение 1
            MES2 – Сообщение 2
            …
            MES9 – Сообщение 9
            BALW- Остаток воды (Гарантированный)
            KOPN – кол-во копеек в монетоприемнике
            GRNN – кол-во гривен в купюроприемнике
            SALW – Воды продано
        */

            
        public DbVendingParam(int _nId , string _sType  , AsaConnection _con, Dictionary<String, String> _param )
        {
         fSum = 0 ;
         nId = _nId ;
         sType = _sType;
         con = _con;
         sStatus = "REQUEST";
         set_command = new ArrayList();


         Param = _param;
         LoadDefaultParam();
        }
        public void LoadDefaultParam()
        {
            Int32 _nId;
            string sql;
            sql = @"select isnull(nPrice1,0) * 100 ,
                           isnull(nPrice2,0)* 100 ,
                            isnull(iWhol,0),
                           isnull(iTara,0),
                           isnull(iDiscr ,0) ,
                            nid ,
                            isnull(ibalw,0) 
                      from apo.tUnits 
                    where nid in (   :a_nid )
                    order by nid";

            AsaCommand cmd = new AsaCommand(sql, con);
            cmd.Parameters.Add("a_nid", nId);
            AsaDataReader dr = cmd.ExecuteReader();
            //def_fPrice1 = System.Convert.ToDecimal(Param["PRICE1"], System.Globalization.CultureInfo.InvariantCulture.NumberFormat) * 100;
            //def_fPrice2 = System.Convert.ToDecimal(Param["PRICE2"], System.Globalization.CultureInfo.InvariantCulture.NumberFormat) * 100;
            while (dr.Read())
            {
                /// read default parameter 
                _nId = dr.GetInt32(5);
          
                    def_fPrice1 = dr.GetDecimal(0);
                    def_fPrice2 = dr.GetDecimal(1);
                    def_iWhol = dr.GetInt32(2); 
                    def_iTara = dr.GetInt32(3);
                    def_iDiscr = dr.GetInt32(4);
                    def_iBalW = dr.GetInt32(6);
          
            }
        }
        public bool ParseParam(string _param )
        {
            DateTime dt;
            dt = DateTime.Now;
            string parmName, parmValue;
            if (_param.Length >= 4)
            {
                parmName = _param.Substring(0, 4);
                parmValue = _param.Substring(5);
            }
            else
            {
                parmName = _param;
                parmValue = "";
            }

            if (parmName.Equals("STAT"))
            {
                sStatus = parmValue;
                return true;
            }

            if (parmName.Equals("ALRM"))
            {
                sAlarm = parmValue;
            }
            if (parmName.Equals("DATE"))
            {  sDate = parmValue;
                ;
               
                set_command.Add("SET_DATE " +  dt.ToString("yy/MM/dd",  new System.Globalization.CultureInfo(String.Empty, false))  );
               return true;    }
            if (parmName.Equals("TIME"))
            {   sTime = parmValue;

                set_command.Add("SET_TIME " + dt.ToString("HH:mm:ss"));
                return true; }
            if (parmName.Equals("PRC1"))
            {   fPrice1  =   Convert.ToDecimal(  parmValue ) ;
            if (fPrice1 != def_fPrice1)
            {
                set_command.Add("SET_PRC1 " + String.Format("{0:####}", def_fPrice1));
            }

                return true;}
            if (parmName.Equals("PRC2"))
            {
                fPrice2 = Convert.ToDecimal(parmValue);
                if (fPrice2 != def_fPrice2)
                    {
                        set_command.Add("SET_PRC2 " + String.Format("{0:####}", def_fPrice2));
                    }

                return true;
            }            
            if (parmName.Equals("WHOL"))
            {
                iWhol = Convert.ToInt32(parmValue);
                  if (iWhol != def_iWhol )
                    {
                        set_command.Add("SET_WHOL " + String.Format("{0:####}", def_iWhol));
                    }

                return true;
            }
            if (parmName.Equals("TARA"))
            {
                iTara = Convert.ToInt32(parmValue);
                if (iTara != def_iTara)
                {
                    set_command.Add("SET_TARA " + String.Format("{0:####}", def_iTara));
                }

                return true;
            }
            if (parmName.Equals("DISR"))
            {
                iDiscr = Convert.ToInt32(parmValue);
                if (iDiscr != def_iDiscr)
                {
                    set_command.Add("SET_DISR " + String.Format("{0:####}", def_iDiscr));
                }

                return true;
            }
            if (parmName.Equals("BALW"))
            {
                iBalW =  Convert.ToInt32(parmValue);
                if ( ! iBalW.Equals(def_iBalW ) ) {
                    set_command.Add("SET_BALW " + String.Format("{0:####}", def_iBalW));
                }
                return true;
            }
            if (parmName.Equals("KOPN"))
            {
                iKopN =  Convert.ToInt32(parmValue);
                Decimal d = Convert.ToDecimal(parmValue); 
                fSum = +(d/100);
                
                return true;
            }
            if (parmName.Equals("GRNN"))
            {
                iGrnN =  Convert.ToInt32(parmValue);
                fSum =+  iGrnN ;
                return true;
            }
               if (parmName.Equals("SALW"))
            {
                fSalW =  Convert.ToDecimal (parmValue);
                
                return true;
            }
            
            return false ;
        }
     
        public void SaveParam()
        {
            string sql = @"insert into apo.tSnapshort
                     (  nid, dt_Action, vtype, nVolumeRest, nVolume ,
                        nsum, nprice1,nprice2,nTara ,nDiscr ,vStatus , vAlarm, 
                        fsalw ,iGrnN, iKopN )
                    values ( :l_nid, current timestamp , :l_vtype ,:l_VolRest,:l_Vol,
                             :l_nsum  ,:l_nprice1, :l_nprice2,:l_nTara, :l_nDiscr , :l_vStatus, :l_valarm ,
                             :l_salw, :l_grn, :l_kop  )";
            AsaCommand cmd = new AsaCommand(sql, con);
            fSum = iGrnN + iKopN / 100;
            cmd.Parameters.Add("l_nid", nId);
            cmd.Parameters.Add("l_vtype", sType);
            cmd.Parameters.Add("l_VolRest", iBalW);
            cmd.Parameters.Add("l_Vol", iWhol);
            cmd.Parameters.Add("l_nsum", fSum);
            cmd.Parameters.Add("l_nprice1", fPrice1);
            cmd.Parameters.Add("l_nprice2", fPrice2);
            cmd.Parameters.Add("l_nTara", iTara);
            cmd.Parameters.Add("l_nDiscr", iDiscr);
            cmd.Parameters.Add("l_vStatus", sStatus);
            cmd.Parameters.Add("l_valarm", sAlarm );

            cmd.Parameters.Add("l_salw", fSalW );
            cmd.Parameters.Add("l_grn", iGrnN);
            cmd.Parameters.Add("l_kop", iKopN);
            cmd.ExecuteNonQuery();

            if (sStatus.Equals("6") && (sAlarm.Equals("1") || sAlarm.Equals("20")) || sType.Equals("RESET") )
            {
                // Обслуживание - записать показания 
                sql = @"insert into apo.Turnover ( nId, dt_Action, nVolume, nSum ) 
                        values ( :l_nid,current timestamp, :l_Vol, :l_nSum )" ;
                cmd = new AsaCommand(sql, con);
                cmd.Parameters.Add("l_nid", nId);
                cmd.Parameters.Add("l_Vol", fSalW);
                cmd.Parameters.Add("l_nSum", fSum);
                cmd.ExecuteNonQuery();

                set_command.Add("SET_KOPN 0");
                set_command.Add("SET_GRNN 0");
                set_command.Add("SET_SALW 0");
                set_command.Add("SET_PRC1 " + String.Format("{0:####}", def_fPrice1));
                set_command.Add("SET_PRC2 " + String.Format("{0:####}", def_fPrice2));
                if ( sType.Equals("RESET") ){
                       set_command.Add("SET_STAT 1");
                }
                else {
                    set_command.Add("SET_STAT 7");
                }
            }
        }
    }
}
