﻿namespace WaterVending
{
    partial class MainWnd
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.sqlca = new Sybase.DataWindow.Transaction(this.components);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tb_items = new System.Windows.Forms.ToolStripButton();
            this.tb_param = new System.Windows.Forms.ToolStripButton();
            this.tb_netconnect = new System.Windows.Forms.ToolStripButton();
            this.tsb_report = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsm_sale = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_request = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tb_items,
            this.tb_param,
            this.tb_netconnect,
            this.tsb_report});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(847, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tb_items
            // 
            this.tb_items.Image = global::WaterVending.Properties.Resources.oper_4;
            this.tb_items.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tb_items.Name = "tb_items";
            this.tb_items.Size = new System.Drawing.Size(102, 22);
            this.tb_items.Text = "Оборудование";
            this.tb_items.Click += new System.EventHandler(this.tb_items_Click);
            // 
            // tb_param
            // 
            this.tb_param.Image = global::WaterVending.Properties.Resources.text;
            this.tb_param.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tb_param.Name = "tb_param";
            this.tb_param.Size = new System.Drawing.Size(84, 22);
            this.tb_param.Text = "Параметры";
            this.tb_param.Click += new System.EventHandler(this.tb_param_Click);
            // 
            // tb_netconnect
            // 
            this.tb_netconnect.Image = global::WaterVending.Properties.Resources.wininet;
            this.tb_netconnect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tb_netconnect.Name = "tb_netconnect";
            this.tb_netconnect.Size = new System.Drawing.Size(60, 22);
            this.tb_netconnect.Text = "Связь ";
            this.tb_netconnect.Click += new System.EventHandler(this.tb_netconnect_Click);
            // 
            // tsb_report
            // 
            this.tsb_report.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsm_sale,
            this.tsm_request});
            this.tsb_report.Image = global::WaterVending.Properties.Resources._1302;
            this.tsb_report.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsb_report.Name = "tsb_report";
            this.tsb_report.Size = new System.Drawing.Size(68, 22);
            this.tsb_report.Text = "Отчет";
            // 
            // tsm_sale
            // 
            this.tsm_sale.Name = "tsm_sale";
            this.tsm_sale.Size = new System.Drawing.Size(175, 22);
            this.tsm_sale.Text = "Отчет по продажам";
            this.tsm_sale.Click += new System.EventHandler(this.tsm_sale_Click);
            // 
            // tsm_request
            // 
            this.tsm_request.Name = "tsm_request";
            this.tsm_request.Size = new System.Drawing.Size(175, 22);
            this.tsm_request.Text = "История опросов";
            this.tsm_request.Click += new System.EventHandler(this.tsm_request_Click);
            // 
            // MainWnd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(847, 626);
            this.Controls.Add(this.toolStrip1);
            this.Name = "MainWnd";
            this.Text = "Сбор и обработка данных";
            this.Load += new System.EventHandler(this.MainWnd_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Sybase.DataWindow.Transaction sqlca;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tb_items;
        private System.Windows.Forms.ToolStripButton tb_param;
        private System.Windows.Forms.ToolStripButton tb_netconnect;
        private System.Windows.Forms.ToolStripDropDownButton tsb_report;
        private System.Windows.Forms.ToolStripMenuItem tsm_sale;
        private System.Windows.Forms.ToolStripMenuItem tsm_request;
    }
}

