﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using Sybase.DataWindow;

namespace WaterVending
{
    public partial class MainWnd : Form
    {
        String ConnectionString;
        
        public MainWnd()
        {
            InitializeComponent();
            ConnectionString = "uid=dba;pwd=sql;dsn=water";
            try
            {
                sqlca.DbParameter = "ConnectString='" + ConnectionString + "'";
                sqlca.Dbms = Sybase.DataWindow.DbmsType.Odbc;
                sqlca.Connect();
            } catch ( Exception e ) {

            }
       
        }

        private void tb_items_Click(object sender, EventArgs e)
        {
            NsiUnits nu = new NsiUnits();
            nu.sqlca = sqlca;
            nu.sDWO = "d_unitsedit";
            nu.Show();
        }

        private void tb_param_Click(object sender, EventArgs e)
        {
            NsiUnits nu1 = new NsiUnits();
            nu1.sqlca = sqlca;
            nu1.sDWO = "d_paramedit";
            nu1.Show();

        }

        private void tb_netconnect_Click(object sender, EventArgs e)
        {
            CallUp cu = new CallUp();
            cu.sqlca = sqlca;
            cu.Show();
        }

        private void MainWnd_Load(object sender, EventArgs e)
        {

        }


        private void tsm_sale_Click(object sender, EventArgs e)
        {
            ReportPreview rp;
            rp = new ReportPreview();
            List<Object> _parm = new List<object>();


            _parm.Add(1);
            rp.Show();
            rp.SetReportParam(ConnectionString, "r_turnover_report", _parm, sqlca);
        }

        private void tsm_request_Click(object sender, EventArgs e)
        {
            ReportPreview rp;
            rp = new ReportPreview();
            List<Object> _parm = new List<object>();


            _parm.Add(1);
            rp.Show();
            rp.SetReportParam(ConnectionString, "r_request_report", _parm, sqlca);

        }
    }
}
