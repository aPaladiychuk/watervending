﻿namespace WaterVending
{
    partial class NsiUnits
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NsiUnits));
            this.sqlca = new Sybase.DataWindow.Transaction(this.components);
            this.dw_units = new Sybase.DataWindow.DataWindowControl();
            this.dwc_control = new WaterVending.dataWindowControl();
            this.SuspendLayout();
            // 
            // dw_units
            // 
            this.dw_units.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dw_units.DataWindowObject = "d_unitsedit";
            this.dw_units.Icon = ((System.Drawing.Icon)(resources.GetObject("dw_units.Icon")));
            this.dw_units.LibraryList = "watervending.pbl";
            this.dw_units.Location = new System.Drawing.Point(3, 32);
            this.dw_units.Name = "dw_units";
            this.dw_units.ScrollBars = Sybase.DataWindow.DataWindowScrollBars.Both;
            this.dw_units.Size = new System.Drawing.Size(830, 526);
            this.dw_units.TabIndex = 0;
            this.dw_units.Text = "dataWindowControl1";
            // 
            // dwc_control
            // 
            this.dwc_control.Location = new System.Drawing.Point(3, -3);
            this.dwc_control.Name = "dwc_control";
            this.dwc_control.Size = new System.Drawing.Size(367, 29);
            this.dwc_control.TabIndex = 1;
            // 
            // NsiUnits
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(835, 562);
            this.Controls.Add(this.dwc_control);
            this.Controls.Add(this.dw_units);
            this.Name = "NsiUnits";
            this.Text = "Перечень оборудования";
            this.Load += new System.EventHandler(this.NsiUnits_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Sybase.DataWindow.DataWindowControl dw_units;
        private dataWindowControl dwc_control;
        public Sybase.DataWindow.Transaction sqlca;
    }
}