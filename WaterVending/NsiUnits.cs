﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;

namespace WaterVending
{
    public partial class NsiUnits : Form
    {
        public string sDWO;
        public NsiUnits()
        {
            InitializeComponent();
        }

        private void NsiUnits_Load(object sender, EventArgs e)
        {
            dwc_control.InitControl(dw_units, sqlca, dataWindowControl.STAT_ADD | dataWindowControl.STAT_DEL |
                                                   dataWindowControl.STAT_SAVE | dataWindowControl.STAT_RETRIEVE);

            
            dw_units.DataWindowObject = sDWO;
            dw_units.SetTransaction(sqlca);
            dw_units.Retrieve();

        }
    }
}
