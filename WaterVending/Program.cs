﻿using System;
using System.Collections.Generic;

using System.Windows.Forms;
using log4net;

namespace WaterVending
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        
        public static readonly ILog log = LogManager.GetLogger(typeof(Program));  
        [STAThread]
        static void Main()
        {
            log4net.Config.DOMConfigurator.Configure();    
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainWnd());
        }
    }
}
