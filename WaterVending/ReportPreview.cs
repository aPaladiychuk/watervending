﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using Sybase.DataWindow;
using iAnywhere.Data.AsaClient;
using log4net;
using log4net.Config;


namespace WaterVending
{
    public partial class ReportPreview : Form
    {
        iAnywhere.Data.AsaClient.AsaConnection connection;
        public static readonly ILog logger = LogManager.GetLogger(typeof(CallUp));
       
        public ReportPreview()
        {
            InitializeComponent();

            try
            {
                connection = new AsaConnection("uid=dba;pwd=sql;dsn=water");
                connection.Open();
            }
            catch (AsaException ae)
            {
                logger.Error(ae.Message, ae);
            }
            catch (Exception ee1)
            {
                logger.Error("Error in Connect " + ee1.Message, ee1);
            }
            AsaCommand cmd = new AsaCommand(@"select nid , isnull( vaddress ,'??' ) || ' ' || vphone as vshowname  from apo.tunits 
                                                where vstat = 'OK'  and nid <> 0 
                                                order by vaddress ", connection);
            AsaDataReader dt = cmd.ExecuteReader();
            lb_Items.DataSource = dt;
            lb_Items.DisplayMember = "vshowname";
            lb_Items.ValueMember = "nid";
        }

        private void ReportPreview_Load(object sender, EventArgs e)
        {

        }
        public void SetReportParam(String  _connection,string Reportname ,  List<Object> _parm, Transaction _sqlca )
        {

            if (_sqlca == null)
            {
                sqlca.DbParameter = "ConnectString='" + _connection + "'";
                sqlca.Dbms = Sybase.DataWindow.DbmsType.Odbc;
                sqlca.Connect();
            }
            else
            {
                sqlca = _sqlca;
            }
            dw_preview.DataWindowObject = Reportname;
            dw_preview.SetTransaction(sqlca);
       

        }

        private void b_print_Click(object sender, EventArgs e)
        {
            dw_preview.PrintDialog();
        }

        private void dw_preview_Click(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String sItems;
            String sName;
            if (cb_All.Checked)
            {
                sItems = "|0|";
            }
            else
            {

                sItems = "";
                var sel = lb_Items.SelectedItems;
                foreach (DataRowView view  in lb_Items.SelectedItems   )
                {
                    sName = view["vshowname"].ToString();
                    sItems += "|"+ view["nid"].ToString() + "|" ;
                }
            }

            List<Object> _parm;
            _parm = new List<object>();

            DateTime ldFrom, ldTill;
            ldFrom = dFrom.Value.Date;
            ldTill = dTill.Value.Date;

            _parm.Add(dFrom.Value.Date);
            _parm.Add(dTill.Value.Date);
            _parm.Add(sItems);

            dw_preview.Retrieve(ldFrom, ldTill, sItems );

        }
    }
}
