﻿namespace WaterVending
{
    partial class ReportPreview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportPreview));
            this.sqlca = new Sybase.DataWindow.Transaction(this.components);
            this.b_print = new System.Windows.Forms.Button();
            this.dFrom = new System.Windows.Forms.DateTimePicker();
            this.dTill = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.dw_preview = new Sybase.DataWindow.DataWindowControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lb_Items = new System.Windows.Forms.ListBox();
            this.cb_All = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // b_print
            // 
            this.b_print.Location = new System.Drawing.Point(3, 0);
            this.b_print.Name = "b_print";
            this.b_print.Size = new System.Drawing.Size(75, 23);
            this.b_print.TabIndex = 1;
            this.b_print.Text = "Печать";
            this.b_print.UseVisualStyleBackColor = true;
            this.b_print.Click += new System.EventHandler(this.b_print_Click);
            // 
            // dFrom
            // 
            this.dFrom.Location = new System.Drawing.Point(397, 3);
            this.dFrom.Name = "dFrom";
            this.dFrom.Size = new System.Drawing.Size(127, 20);
            this.dFrom.TabIndex = 3;
            // 
            // dTill
            // 
            this.dTill.Location = new System.Drawing.Point(580, 3);
            this.dTill.Name = "dTill";
            this.dTill.Size = new System.Drawing.Size(122, 20);
            this.dTill.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(530, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "по";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(378, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "с";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(728, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Обновить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dw_preview
            // 
            this.dw_preview.DataWindowObject = "";
            this.dw_preview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dw_preview.Icon = ((System.Drawing.Icon)(resources.GetObject("dw_preview.Icon")));
            this.dw_preview.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.dw_preview.LibraryList = "watervending.pbl";
            this.dw_preview.Location = new System.Drawing.Point(0, 0);
            this.dw_preview.Name = "dw_preview";
            this.dw_preview.ScrollBars = Sybase.DataWindow.DataWindowScrollBars.Both;
            this.dw_preview.Size = new System.Drawing.Size(723, 581);
            this.dw_preview.TabIndex = 0;
            this.dw_preview.Text = "dataWindowControl1";
            this.dw_preview.Click += new System.EventHandler(this.dw_preview_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.dw_preview);
            this.panel1.Location = new System.Drawing.Point(202, 29);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(723, 581);
            this.panel1.TabIndex = 2;
            // 
            // lb_Items
            // 
            this.lb_Items.FormattingEnabled = true;
            this.lb_Items.Location = new System.Drawing.Point(3, 56);
            this.lb_Items.Name = "lb_Items";
            this.lb_Items.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lb_Items.Size = new System.Drawing.Size(193, 537);
            this.lb_Items.TabIndex = 8;
            // 
            // cb_All
            // 
            this.cb_All.AutoSize = true;
            this.cb_All.Location = new System.Drawing.Point(12, 33);
            this.cb_All.Name = "cb_All";
            this.cb_All.Size = new System.Drawing.Size(108, 17);
            this.cb_All.TabIndex = 9;
            this.cb_All.Text = "По всем точкам";
            this.cb_All.UseVisualStyleBackColor = true;
            // 
            // ReportPreview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(921, 608);
            this.Controls.Add(this.cb_All);
            this.Controls.Add(this.lb_Items);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dTill);
            this.Controls.Add(this.dFrom);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.b_print);
            this.Name = "ReportPreview";
            this.Text = "ReportPreview";
            this.Load += new System.EventHandler(this.ReportPreview_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Sybase.DataWindow.Transaction sqlca;
        private System.Windows.Forms.Button b_print;
        private System.Windows.Forms.DateTimePicker dFrom;
        private System.Windows.Forms.DateTimePicker dTill;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private Sybase.DataWindow.DataWindowControl dw_preview;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListBox lb_Items;
        private System.Windows.Forms.CheckBox cb_All;
    }
}