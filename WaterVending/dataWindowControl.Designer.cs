﻿namespace WaterVending
{
    partial class dataWindowControl
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.bt_add = new System.Windows.Forms.Button();
            this.bt_Save = new System.Windows.Forms.Button();
            this.bt_Retrieve = new System.Windows.Forms.Button();
            this.bt_del = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // bt_add
            // 
            this.bt_add.Location = new System.Drawing.Point(3, 3);
            this.bt_add.Name = "bt_add";
            this.bt_add.Size = new System.Drawing.Size(75, 23);
            this.bt_add.TabIndex = 0;
            this.bt_add.Text = "Добавить";
            this.bt_add.UseVisualStyleBackColor = true;
            this.bt_add.Click += new System.EventHandler(this.bt_add_Click);
            // 
            // bt_Save
            // 
            this.bt_Save.Location = new System.Drawing.Point(97, 3);
            this.bt_Save.Name = "bt_Save";
            this.bt_Save.Size = new System.Drawing.Size(75, 23);
            this.bt_Save.TabIndex = 1;
            this.bt_Save.Text = "Сохранить";
            this.bt_Save.UseVisualStyleBackColor = true;
            this.bt_Save.Click += new System.EventHandler(this.bt_Save_Click);
            // 
            // bt_Retrieve
            // 
            this.bt_Retrieve.Location = new System.Drawing.Point(286, 3);
            this.bt_Retrieve.Name = "bt_Retrieve";
            this.bt_Retrieve.Size = new System.Drawing.Size(75, 23);
            this.bt_Retrieve.TabIndex = 2;
            this.bt_Retrieve.Text = "Обновить";
            this.bt_Retrieve.UseVisualStyleBackColor = true;
            this.bt_Retrieve.Click += new System.EventHandler(this.bt_Retrieve_Click);
            // 
            // bt_del
            // 
            this.bt_del.Location = new System.Drawing.Point(193, 3);
            this.bt_del.Name = "bt_del";
            this.bt_del.Size = new System.Drawing.Size(75, 23);
            this.bt_del.TabIndex = 3;
            this.bt_del.Text = "Удалить";
            this.bt_del.UseVisualStyleBackColor = true;
            this.bt_del.Click += new System.EventHandler(this.bt_del_Click);
            // 
            // dataWindowControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.bt_del);
            this.Controls.Add(this.bt_Retrieve);
            this.Controls.Add(this.bt_Save);
            this.Controls.Add(this.bt_add);
            this.Name = "dataWindowControl";
            this.Size = new System.Drawing.Size(367, 29);
            this.Load += new System.EventHandler(this.dwtaWindowControl_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bt_add;
        private System.Windows.Forms.Button bt_Save;
        private System.Windows.Forms.Button bt_Retrieve;
        private System.Windows.Forms.Button bt_del;
    }
}
