﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;

using System.Text;
using System.Windows.Forms;
using Sybase.DataWindow;

namespace WaterVending
{
    public partial class dataWindowControl : UserControl
    {
        public static int STAT_ADD      = 1 ;
        public static int STAT_DEL = 2;
        public static int STAT_RETRIEVE = 4;
        public static int STAT_SAVE = 8;

        private    DataWindowControl ldw_parent;
        private Transaction sqlca;
        public object[] _args;
        public dataWindowControl()
        {
            InitializeComponent();
        }

        private void dwtaWindowControl_Load(object sender, EventArgs e)
        {

        }
        public void InitControl(DataWindowControl _parent, Transaction _sqlca , int _flags)
        {
            ldw_parent = _parent;
            sqlca = _sqlca;

            if ((_flags & STAT_ADD) == 0)  bt_add.Visible = false;
            if ((_flags & STAT_DEL) == 0) bt_del.Visible = false;
            if ((_flags & STAT_RETRIEVE) == 0) bt_Retrieve.Visible = false;
            if ((_flags & STAT_SAVE) == 0) bt_Save.Visible = false;
            _args = new object[0];
        }

        private void bt_add_Click(object sender, EventArgs e)
        {
            int row ;
            row = ldw_parent.InsertRow(0);
            ldw_parent.SetRow(row);
            ldw_parent.ScrollToRow(row);
            ldw_parent.Focus();
        }

        private void bt_Save_Click(object sender, EventArgs e)
        {
            ldw_parent.AcceptText();
            ldw_parent.UpdateData();
            sqlca.Commit();
        }

        private void bt_del_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Вы действительно хотите удалить строку ?", "Предупреждение", MessageBoxButtons.YesNo)
                        == DialogResult.Yes)
            {
                ldw_parent.DeleteRow(ldw_parent.CurrentRow);
            }
            
        }

        private void bt_Retrieve_Click(object sender, EventArgs e)
        {
            ldw_parent.Retrieve(_args);
            sqlca.Commit();
        }
      

    }
}
