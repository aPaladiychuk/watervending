/*==============================================================*/
/* DBMS name:      Sybase AS Anywhere 9                         */
/* Created on:     17.01.2013 22:43:17                          */
/*==============================================================*/


if exists(select 1 from sys.sysforeignkey where role='FK_TURNOVER_FK_TURN_B_TUNITS') then
    alter table APO.Turnover
       delete foreign key FK_TURNOVER_FK_TURN_B_TUNITS
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_SESSION__FK_UNIT_I_TUNITS') then
    alter table APO.session_log
       delete foreign key FK_SESSION__FK_UNIT_I_TUNITS
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_TSNAPSHO_FK_SNAPSH_TUNITS') then
    alter table APO.tSnapshort
       delete foreign key FK_TSNAPSHO_FK_SNAPSH_TUNITS
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='FK_TURN_BY_UNIT_FK'
     and t.table_name='Turnover'
     and i.creator=user_id('APO')
) then
   drop index APO.Turnover.FK_TURN_BY_UNIT_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='Turnover_PK'
     and t.table_name='Turnover'
     and i.creator=user_id('APO')
) then
   drop index APO.Turnover.Turnover_PK
end if;

if exists(
   select 1 from sys.systable 
   where table_name='Turnover'
     and table_type in ('BASE', 'GBL TEMP')
     and creator=user_id('APO')
) then
    drop table APO.Turnover
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='FK_UNIT_IN_LOG_FK'
     and t.table_name='session_log'
     and i.creator=user_id('APO')
) then
   drop index APO.session_log.FK_UNIT_IN_LOG_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='session_log_PK'
     and t.table_name='session_log'
     and i.creator=user_id('APO')
) then
   drop index APO.session_log.session_log_PK
end if;

if exists(
   select 1 from sys.systable 
   where table_name='session_log'
     and table_type in ('BASE', 'GBL TEMP')
     and creator=user_id('APO')
) then
    drop table APO.session_log
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='tParameter_PK'
     and t.table_name='tParameter'
     and i.creator=user_id('APO')
) then
   drop index APO.tParameter.tParameter_PK
end if;

if exists(
   select 1 from sys.systable 
   where table_name='tParameter'
     and table_type in ('BASE', 'GBL TEMP')
     and creator=user_id('APO')
) then
    drop table APO.tParameter
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='FK_SNAPSHORT_BY_UNIT_FK'
     and t.table_name='tSnapshort'
     and i.creator=user_id('APO')
) then
   drop index APO.tSnapshort.FK_SNAPSHORT_BY_UNIT_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='tSnapshort_PK'
     and t.table_name='tSnapshort'
     and i.creator=user_id('APO')
) then
   drop index APO.tSnapshort.tSnapshort_PK
end if;

if exists(
   select 1 from sys.systable 
   where table_name='tSnapshort'
     and table_type in ('BASE', 'GBL TEMP')
     and creator=user_id('APO')
) then
    drop table APO.tSnapshort
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='tUnits_PK'
     and t.table_name='tUnits'
     and i.creator=user_id('APO')
) then
   drop index APO.tUnits.tUnits_PK
end if;

if exists(
   select 1 from sys.systable 
   where table_name='tUnits'
     and table_type in ('BASE', 'GBL TEMP')
     and creator=user_id('APO')
) then
    drop table APO.tUnits
end if;

revoke connect from APO;

/*==============================================================*/
/* User: APO                                                    */
/*==============================================================*/
grant connect to APO identified by "";

/*==============================================================*/
/* Table: Turnover                                              */
/*==============================================================*/
create table APO.Turnover 
(
    nTurnover            integer                        not null,
    nId                  integer                        null,
    dt_Action            timestamp                      null,
    nVolume              numeric(12,3)                  null,
    nSum                 numeric(12,2)                  null,
    constraint PK_TURNOVER primary key (nTurnover)
);

/*==============================================================*/
/* Index: Turnover_PK                                           */
/*==============================================================*/
create unique index Turnover_PK on APO.Turnover (
nTurnover ASC
);

/*==============================================================*/
/* Index: FK_TURN_BY_UNIT_FK                                    */
/*==============================================================*/
create  index FK_TURN_BY_UNIT_FK on APO.Turnover (
nId ASC
);

/*==============================================================*/
/* Table: session_log                                           */
/*==============================================================*/
create table APO.session_log 
(
    nTransaction         integer                        not null,
    nId                  integer                        null,
    dtAction             timestamp                      null,
    vAction              varchar(50)                    null,
    vStatus              varchar(50)                    null,
    vGround              varchar(1024)                  null,
    constraint PK_SESSION_LOG primary key (nTransaction)
);

/*==============================================================*/
/* Index: session_log_PK                                        */
/*==============================================================*/
create unique index session_log_PK on APO.session_log (
nTransaction ASC
);

/*==============================================================*/
/* Index: FK_UNIT_IN_LOG_FK                                     */
/*==============================================================*/
create  index FK_UNIT_IN_LOG_FK on APO.session_log (
nId ASC
);

/*==============================================================*/
/* Table: tParameter                                            */
/*==============================================================*/
create table APO.tParameter 
(
    nParam               integer                        not null,
    vParamName           varchar(255)                   null,
    vType                varchar(20)                    null,
    vValue               varchar(255)                   null,
    constraint PK_TPARAMETER primary key (nParam)
);

/*==============================================================*/
/* Index: tParameter_PK                                         */
/*==============================================================*/
create unique index tParameter_PK on APO.tParameter (
nParam ASC
);

/*==============================================================*/
/* Table: tSnapshort                                            */
/*==============================================================*/
create table APO.tSnapshort 
(
    nSnId                integer                        not null,
    nId                  integer                        null,
    dt_Action            timestamp                      null,
    vType                varchar(20)                    null,
    nVolumeRest          numeric(12,3)                  null,
    nVolume              numeric(12,3)                  null,
    nSum                 numeric(12,2)                  null,
    constraint PK_TSNAPSHORT primary key (nSnId)
);

/*==============================================================*/
/* Index: tSnapshort_PK                                         */
/*==============================================================*/
create unique index tSnapshort_PK on APO.tSnapshort (
nSnId ASC
);

/*==============================================================*/
/* Index: FK_SNAPSHORT_BY_UNIT_FK                               */
/*==============================================================*/
create  index FK_SNAPSHORT_BY_UNIT_FK on APO.tSnapshort (
nId ASC
);

/*==============================================================*/
/* Table: tUnits                                                */
/*==============================================================*/
create table APO.tUnits 
(
    nId                  integer                        not null,
    vName                varchar(255)                   null,
    vSerNo               varchar(100)                   null,
    vAddress             varchar(1024)                  null,
    vStat                varchar(50)                    null,
    vPhone               varchar(15)                    null,
    constraint PK_TUNITS primary key (nId)
);

/*==============================================================*/
/* Index: tUnits_PK                                             */
/*==============================================================*/
create unique index tUnits_PK on APO.tUnits (
nId ASC
);

alter table APO.Turnover
   add constraint FK_TURNOVER_FK_TURN_B_TUNITS foreign key (nId)
      references APO.tUnits (nId)
      on update restrict
      on delete restrict;

alter table APO.session_log
   add constraint FK_SESSION__FK_UNIT_I_TUNITS foreign key (nId)
      references APO.tUnits (nId)
      on update restrict
      on delete restrict;

alter table APO.tSnapshort
   add constraint FK_TSNAPSHO_FK_SNAPSH_TUNITS foreign key (nId)
      references APO.tUnits (nId)
      on update restrict
      on delete restrict;

